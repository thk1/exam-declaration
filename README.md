 [![License: MIT](https://img.shields.io/badge/License-MIT-yellow.svg)](https://opensource.org/licenses/MIT)  
 [![pipeline status](https://gitlab.com/thk1/exam-declaration/badges/master/pipeline.svg)](https://gitlab.com/thk1/exam-declaration/-/commits/master)  
 [![Website shields.io](https://img.shields.io/website-up-down-green-red/http/shields.io.svg)](https://thk1.gitlab.io/exam-declaration)  
 [![made-with-latex](https://img.shields.io/badge/Made%20with-LaTeX-1f425f.svg)](https://www.latex-project.org/)  
 [![made-with-python](https://img.shields.io/badge/Made%20with-Python-1f425f.svg)](https://www.python.org/)
 
(This form was obsoleted by the new [official form](https://www.th-koeln.de/mam/downloads/deutsch/studium/rundumsstudium/organisation/eigenstandigkeitserklarung_zur_prufung.pdf).) 

# No cheat declarations for remote examinations

Due to the [COVID-19
pandemic](https://en.wikipedia.org/wiki/COVID-19_pandemic) exams at
the [faculty of applied
sciences](https://www.th-koeln.de/en/applied-natural-sciences/faculty-of-applied-natural-sciences_6824.php)
[TH Köln](https://www.th-koeln.de) are supposed to be taken remotely, whenever possible.

The means of preventing use of non admissible examination aids are
distressingly limited under these circumstances. The suggested
solution is, of course, to put no restrictions on the use of auxiliary
resources. But even this approach is limited if it comes to help
by artificial intelligence or even other people.

One of the few remaining means are *declarations* in which students –
under the [threat of civil
penalty](https://recht.nrw.de/lmi/owa/br_bes_detail?sg=0&menu=1&bes_id=28364&anw_nr=2&aufgehoben=N&det_id=469605)
– must affirm after sitting an examination that they did not use any
unauthorised aids.

# Forms

## Declarations for summer 2020

| couse | module     | form |
| ----- | ---------- | ---- |
| AC | Allgemeine Chemie                                  | https://thk1.gitlab.io/exam-declaration/AC_1110.pdf |
| AC | Anorganische Chemie I                              | https://thk1.gitlab.io/exam-declaration/AC_1410.pdf |
| AC | Anorganische Chemie II                             | https://thk1.gitlab.io/exam-declaration/AC_2410.pdf |
| PC | Allgemeine und Anorganische Chemie                 | https://thk1.gitlab.io/exam-declaration/PC_1110.pdf |
| PC | PharmBasics                                        | https://thk1.gitlab.io/exam-declaration/PC_1210.pdf |
| PC | Mathematik                                         | https://thk1.gitlab.io/exam-declaration/PC_1410.pdf |
| PC | Physikalische Chemie und Physikalische Pharmazie   | https://thk1.gitlab.io/exam-declaration/PC_2510.pdf |
| PC | Bio-Pharmazeutische Chemie                         | https://thk1.gitlab.io/exam-declaration/PC_4110.pdf |

## Declaration for winter 2021

The current forms are published [here](https://thk1.gitlab.io/exam-declaration).

# LaTeX class

The *declaration* class provides a simple form for exam
declarations in the corporate design of TH Köln.

## LaTeX header

The following macros can be used to set the attributes of the examination:

| macro | argument | default value |
| ----- | -------- | ------------- |
| `\logo` | macro for typesetting a logo or name of the university | `\THlogo` |
| `\faculty`| name of the faculty | Fakultät für Angewandte Naturwissenschaften |
| `\module` | name of the course |
| `\date` | date of the examination |
| `\aid` | permitted tool |

Admissible aids are declared with the `\aid` macro. Each `aid`
statement results in an entry to an unordered list.

The LaTeX header for a declaration form looks like this:

```
\documentclass{declaration}

\date{7. Juli 2020}
\module{Mathematik}

% admissible aids
\aid{leeres Papier,}
\aid{Stifte, Lineal/Geodreieck,}
\aid{Taschenrechner (nicht programmierbar),}
\aid{selbst zusammengestellte, schriftliche Sammlung von Materialien zur Vorlesung,}
\aid{Formelsammlung, Lehrbücher (auch als \textsl{ebook}).}
```

## LaTeX body

In the body of the document the following macros can be used:

| macro | purpose |
| ----- | -------- |
| `\thelogo` | prints the logo |
| `\thefaculty` | prints the name of the faculty |
| `\themodule` | prints the name of the course |
| `\thedate` | prints the date of the examination |
| `\showaids` | prints an (unordered) list of admissible examination aids |
| `\namematno` | prints form fields for the name and matricualation number of an examinee |
| `\signatureline` | prints form fields for location, date and signature |
| `\these` | results in `keine` if there are no `aids` declarations, else in `folgende` |
|          | (or the respective words in other supported languages) |


# Python template

It is possible to generate the LaTeX source with [Python](https://www.python.org] (e. g. via
[pdflatex](https://pypi.org/project/pdflatex/) or
[Jinja2](https://jinja.palletsprojects.com/en/2.11.x/) templates).

Python can even create a PDF file by means of the
[pdflatex](https://pypi.org/project/pdflatex/) library.

So it is possible for users that are not experienced in the use of
LaTeX to generate their forms, e. g. through a [Jupyter
notebook](https://jupyter.org/) or even a [GUI
application](https://wiki.python.org/moin/GuiProgramming).

## `declaration.py`

This is a Python script that takes a [YAML](https://yaml.org) file
with meta data for the examination and a list of admissible aids and
generates a corresponding form as PDF file.

### YAML files

The input files for `declaration.py` look like this:

``` yaml
%YAML 1.2
---
# metadata
module: 
  course: PC
  title: Mathematik

examination:
  number: 1410
  date: 2020-08-20
  language: de-DE
  aids: # list all allowed examination tools here
  - leeres Papier,
  - Stifte, Lineal/Geodreieck,
  - Taschenrechner (nicht programmierbar),
  - selbst zusammengestellte, schriftliche Sammlung von Materialien zur Vorlesung,
  - Formelsammlung, Lehrbücher (auch als \textsl{ebook}).
```

# TODO

- read input files from command line in `mkindex`
- split build process into Python and TeX parts
- create docker images for build processes
- place definition of docker images in separate repository

- support documents in English (as *proof of concept*)?
