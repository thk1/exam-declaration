#! /usr/bin/env python3
# -*- coding: utf-8 -*-

""" create index of declaration forms from YAML directory in HTML format """

import yaml
from jinja2 import Template
import os, sys

import logging
logging.basicConfig(level=logging.DEBUG)

class DeclarationIndex():

    url_root = 'https://thk1.gitlab.io/exam-declaration'
    template = Template(
"""<!doctype html>

<html lang="en">
<head>
  <meta charset="utf-8">

  <title>No Cheat Declaration</title>
  <meta name="author" content="(Fakultät für Angewandte Naturwissenschaften TH Köln)"/>
  <meta name="description" content="non cheating declarations for exams">

  <link rel="icon" type="image/png" sizes="32x32" href="https://www.th-koeln.de/favicon-32x32.png">

  <style> body { font-family: Arial, sans-serif; } </style>
</head>
<body>
  <div>
    <div style="display: inline-block;">
    <img alt="Logo der TH Köln" 
         width="100px"
         src="https://www.th-koeln.de/img/logo.svg">
    </div>
    <div style="display: inline-block; vertical-align: top; margin-left: 50px">
      &nbsp;<em>Fakultät für Angewandte Naturwissenschaften</em>
    </div>
  </div>
  <h1>Versicherungen zu elektronischen Prüfungen</h1>
  <h2>Formulare</h2>
    <ul id="table">
    {% for item in declarations %}
        <li>{{ item.course }}&nbsp;&ndash;&nbsp;
            {{ item.date }}&nbsp;&nbsp;&ndash;&nbsp;
            <a href="{{ item.url }}">{{ item.title }}</a>
        </li>
    {% endfor %}
    </ul>
</body>
</html>
""")

    def __init__(self, ydir='../YAML', template='index.html'):

        self.ydir = ydir
        self.exams = []
        self.get_exams()
        self.declarations = [self.get_declaration_data(e) for e in self.exams]

    def get_exams(self):
        for root, dirs, files in os.walk(self.ydir):
            for f in sorted(files):
                if not f.endswith('yaml'):
                    logging.debug(f'skipping {f} ...')
                    continue
                logging.info(f'processing {f} ...')
                ypath = os.path.join(root, f)
                self.get_yaml(ypath)

    def get_yaml(self, path):
        with open(path, 'r') as stream:
            exam = yaml.safe_load(stream)

            self.exams.append(exam)

    def get_declaration_data(self, exam):
        course  = exam['module']['course']
        title   = exam['module']['title']
        ex_no   = exam['examination']['number']
        ex_date = exam['examination']['date']
        url     = f'{self.url_root}/{course}_{ex_no}.pdf'
        return dict(
            date   = ex_date,
            course = course,
            title  = title,
            url    = url)

    def html_index(self):
        html = self.template.render(declarations = self.declarations)
        print(html)

    def markdown_table(self):
        """ 
        create table data for README.md 
        """

        for exam in self.exams:
            decl_data = self.get_declaration_data(exam)
            print(f"| {decl_data['course']:2} | {decl_data['title']:50} | {decl_data['url']} |")

if __name__ == '__main__':
        index = DeclarationIndex()
        index.html_index()
        
