\NeedsTeXFormat{LaTeX2e}
\ProvidesClass{declaration}[2020/07/03 statutory declaration LaTeX class]

\LoadClass[12pt, a4paper]{article}

\RequirePackage[T1]{fontenc}
\RequirePackage{titling}
\RequirePackage{parskip}

%% Font
\RequirePackage{helvet}
\renewcommand{\familydefault}{\sfdefault}

%% Logo
\RequirePackage{graphicx}
\newlength{\@logoheight}\setlength{\@logoheight}{16mm}

%% Language support
\RequirePackage{polyglossia}
\setdefaultlanguage{german}
\gappto\captionsgerman{\def\declarationname{Erklärung zur Modulprüfung}}
\gappto\captionsgerman{\def\thesename{folgende}}
\gappto\captionsgerman{\def\nonename{keine}}
\gappto\captionsenglish{\def\declarationname{Declaration for Examination}}
\gappto\captionsenglish{\def\thesename{the following}}
\gappto\captionsenglish{\def\nonename{no}}
\def\thedate{\mbox{\today}}

%% Page layout
\RequirePackage[left=2cm,right=1.5cm,top=41mm,bottom=15mm,headheight=50pt,headsep=20mm]{geometry}
\RequirePackage{fancyhdr}
\pagestyle{fancy}
\fancyhf{}
\lhead{\includegraphics[height=\@logoheight]{Logo_TH-Koeln_RGB_17pt.jpg}}
\chead{\thefaculty}
\cfoot{\footnotesize \textbf{Original bitte aufbewahren} und auf Verlangen vorlegen!}

%% Form fields
\RequirePackage{xcolor}
\RequirePackage{hyperref}
% \hypersetup{bordercolor={black}, borderstyle=u}
\def\DefaultHeightofText{8mm}

\setlength{\unitlength}{1mm}
\setlength{\parindent}{0pt}

\renewcommand\subsection{\@startsection{subsection}{2}{\z@}%
                                     {-3.25ex\@plus -1ex \@minus -.2ex}%
                                     {1.5ex \@plus .2ex}%
                                     {\normalfont\large\bfseries}}
\newcommand{\namematno}{%
  \begin{Form}%
  \begin{picture}(169,30)(0, -10)
    \put(  0, 0){\TextField[width=85mm, bordercolor={black}]{Name:}}
    \put(110, 0){\TextField[width=40mm, bordercolor={black}]{Matrikelnr.:}}
  \end{picture}%
  \end{Form}%
}
\newcommand{\signatureline}{%
  \begin{Form}
  \setlength{\unitlength}{1mm}
  \begin{picture}(180, 22)\scriptsize
    \put( 0, 3){\TextField[width=80mm, bordercolor={black}]{}}
    \put( 0, 0){\makebox{Ort, Datum}}
    \put(90, 0){%
      \put(0, 3){\line(1, 0){85}} % Unterschrift
      \put(0, 0){Unterschrift}}
  \end{picture}%
  \end{Form}
}

%% logo
\def\thelogo{\THlogo}
\newcommand{\logo}[1]{\def\thelogo{#1}}

%% faculty name
\def\thefaculty{Fakultät für Angewandte Naturwissenschaften}
\newcommand{\fraculty}[1]{\def\thefaculty{#1}}

%% module name
\newcommand{\module}[1]{\def\themodule{#1}}

\def\none{none}
\def\these{\nonename}
\def\theaids{}
\newcommand{\aid}[1]{%
  \def\these{\thesename}
  \expandafter\def\expandafter\theaids\expandafter{\theaids \item #1}
}
\newcommand{\showaids}{%
  \ifx\these\none.\else:
  \begin{itemize}
    \theaids
  \end{itemize}
  \fi\bigskip
}

\renewcommand{\maketitle}{%
  \vspace*{-2cm}
  \begin{center}
    \textbf{\large \declarationname\ \themodule}
  \end{center}
  \namematno
}

% declaration.cls ends here
\endinput
% Local Variables:
% TeX-master: "PC_1410.tex"
% TeX-engine: xetex
% End:
