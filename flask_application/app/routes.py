from flask import render_template, flash, redirect, url_for, send_from_directory
from os import path

from app import app
from app.forms import AidsForm

from .declaration import Declaration

@app.route('/', methods=['GET', 'POST'])
@app.route('/aids', methods=['GET', 'POST'])
def aids():
    form = AidsForm()
    if form.validate_on_submit():
        declaration = Declaration(form)
        declaration.dump_yaml()
        declaration.create_PDF()
        fname = declaration.get_PDF_filename()
        # flash(f'file <a href="{fname}">{fname}</a> created')
        flash(f'file {fname} created')
        return redirect(fname)
    return render_template('aids.html', title='erlaubte Hilfsmittel', form=form)
    
@app.route('/artifacts/<path:pdffile>')
def serve_pdf(pdffile):
    print(f'serving {pdffile}')
    try:
        return send_from_directory(f"../{app.config['ARTIFACTS']}", pdffile, as_attachment=True)
    except Exception as e:
        return str(e)
