#! /usr/bin/env python3
# -*- coding: utf-8 -*-

""" generate forms for statutory declarations for our exams using templates """

import yaml
import jinja2
import os, sys

from app import app

class Declaration():

    template = r"""% -*- mode: LaTeX; TeX-engine: xetex -*-
\documentclass[de-DE]{declaration}

\newcommand{\VAR}[1]{}
\newcommand{\BLOCK}[1]{1}

\module{\VAR{module.title}}
\day=\VAR{examination.date.strftime('%d')}
\month=\VAR{examination.date.strftime('%m')}
\year=\VAR{examination.date.strftime('%Y')}

\BLOCK{ for aid in examination.aids }
\aid{\VAR{aid}}
\BLOCK{ endfor }

\begin{document}
  \input{app/templates/DE}
\end{document}
"""
    
    def __init__(self, form=None, yfile=None):
        self.texed = False
        self.data = dict(module={}, examination={})
        
        self.get_template()
        if yfile is not None:
            self.get_yaml()
        if form is not None:
            self.data['module']['course'] = form.course.data
            self.data['module']['title'] = form.module.data
            self.data['examination']['date'] = form.date.data
            self.data['examination']['number'] = form.number.data
            self.data['examination']['aids'] = [aid for aid in form.aids.data if aid]

        self.basename = f"{self.data['module']['course']}_{self.data['examination']['number']}"
        self.path = os.path.join(app.config['ARTIFACTS'], self.basename)

        if not os.path.exists(app.config['ARTIFACTS']):
            os.makedirs(app.config['ARTIFACTS'])

    def get_template(self):
        latex_jinja_env = jinja2.Environment(
            block_start_string = '\BLOCK{',
            block_end_string = '}',
            variable_start_string = '\VAR{',
            variable_end_string = '}',
            comment_start_string = '\#{',
            comment_end_string = '}',
            line_statement_prefix = '%-',
            line_comment_prefix = '%#',
            trim_blocks = True,
            autoescape = False,
            loader = jinja2.FileSystemLoader(os.path.abspath('.'))
        )
        self.template = latex_jinja_env.from_string(self.template)

    def get_yaml(self):
        if not self.basename:
            self.data = None
        with open(self.get_filename("yaml"), 'r') as stream:
            self.data = yaml.safe_load(stream)

    def dump_yaml(self):
        with open(self.get_filename('yaml'), 'w') as f:
            yaml.dump(self.data, f)

    def get_filename(self, ext):
        return f"{self.path}.{ext}"

    def get_PDF_filename(self):
        return self.get_filename("pdf")

    def create_TeX(self):
        with open(self.get_filename('tex'), 'w') as tex:
            tex.write( self.template.render(**self.data) )
        self.texed = True

    def create_PDF(self):
        if not self.texed:
            self.create_TeX()
        os.system(f"xelatex -interaction=batchmode -output-directory {app.config['ARTIFACTS']} {self.path}")


if __name__ == '__main__':
    if len(sys.argv) < 2:
        sys.exit(f'usage: {sys.argv[0]} exam.yaml [exam.yaml [exam.yaml ...]]')

    for arg in sys.argv[1:]:
        form = Declaration(yfile=arg)
        form.create_PDF()
        
