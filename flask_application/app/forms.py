from flask_wtf import FlaskForm
from wtforms import DateField, RadioField, StringField, SubmitField, FieldList
from wtforms.validators import DataRequired
from datetime import datetime

class AidsForm(FlaskForm):
    course = RadioField('Studiengang: ', 
                        choices=[('AC', 'Angewandte Chemie'),
                                 ('PC', 'Pharmazeutische Chemie'),
                                 ('TC', 'Technische Chemie'),
                                 ('ACM', 'Angewandte Chemie (Master)'),
                                 ('DDD', 'Drug Discovery and Development (Master)')])
    module = StringField('Modultitel', default='', validators=[DataRequired()])
    number = StringField('Nummer', validators=[DataRequired()])
    date   = DateField('Datum',
                       format="%Y-%m-%d",
                       default=datetime.today,
                       validators=[DataRequired()])
    aids   = FieldList(StringField('-'),
                       label='erlaubte Hilfsmittel',
                       min_entries=6, max_entries=6)
